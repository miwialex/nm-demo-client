import React, { Component } from 'react';
import { branch } from 'baobab-react/higher-order';
import { FormControl } from 'react-bootstrap';

import { updateGenre } from '../actions';

class GenreSelect extends Component {

    handleChange(event) {
		const genre = event.target.value
		this.props.dispatch(
			updateGenre,
			genre
		);
	}

    render() {
        const { currentGenres, currentGenre, hidden } = this.props;
        let options = null;
		if (currentGenres && !hidden) {
			options = currentGenres.map(option => { return <option key={ option.id } value={ option.id }>{ option.name }</option>; })
			options.unshift(<option key={0} value="">All</option>)
		}
        return (
            <FormControl 
                className={ hidden ? 'hidden' : '' }
                componentClass="select"
                placeholder="Genre"
                value={ currentGenre }
                onChange={ e => this.handleChange(e) }>
                { options }
            </FormControl>
        )
    }
}

export default branch({
    currentGenres: ['currentGenres'],
}, GenreSelect);