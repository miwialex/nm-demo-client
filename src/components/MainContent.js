import React from 'react';
import { Row, Col } from 'react-bootstrap';
import SearchForm from './SearchForm';
import Results from './Results';

const MainContent = () => {
	return (
		<Row>
			<Col sm={ 12 } md={ 3 }>
				<SearchForm />
			</Col>
			<Col sm={ 12 } md={ 9 }>
				<Results />
			</Col>
		</Row>
	);
}

export default MainContent;