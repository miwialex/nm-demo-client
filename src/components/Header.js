import React from 'react';
import { Col, Row, Jumbotron } from 'react-bootstrap';


const Header = () => {
	return (
		<Row>
			<Col sm={ 12 }>
				<Jumbotron>
					<h1>Movie Finder!</h1>
					<p>This is my demo app for Northwestern Mutal. Search for a movie title, or key word. You can also then select a genre to narrow filter the results</p>
				</Jumbotron>
			</Col>
		</Row>
	)
};

export default Header;
