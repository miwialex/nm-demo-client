import React from 'react';
import { Button, ControlLabel, Form, FormGroup, FormControl } from 'react-bootstrap';
import { branch } from 'baobab-react/higher-order';
import { search } from '../actions';

class SearchForm extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			searchString: null
		}
		
	}

	handleChange(event) {
		this.setState({
			searchString: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		const searchString = this.state.searchString;
		if (searchString && searchString.length > 0) {
			this.props.dispatch(
				search,
				searchString
			);
		}
	}

	getValidationState() {
		const searchString = this.state.searchString;
		if (searchString === null) {
			return null;
		} else {
			return searchString.length > 0 ? 'success' : 'error';
		}
	}

	render() {
		return (
			<Form onSubmit={ e => this.handleSubmit(e) }>
				<FormGroup validationState={ this.getValidationState() }>
					<ControlLabel>Search For A Movie</ControlLabel>
					<FormControl
						type="text"
						value={ this.state.searchString || '' }
						placeholder="Enter Text"
						onChange={ e => this.handleChange(e) }/>
				</FormGroup>
				<Button type="submit" block>
					Search
				</Button>
			</Form>
		);
	}
};

export default branch({}, SearchForm);