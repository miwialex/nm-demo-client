import React from 'react';
import { Col, Row } from 'react-bootstrap';

const Footer = () => {
	const year = new Date().getFullYear();

	return (
		<Row>
			<Col sm={ 12 }>
				<hr/>
				<footer>&copy;{year} · Ruben Manrique</footer>
			</Col>
		</Row>
	)
};

export default Footer;