import React from 'react';
import { chunk, filter } from 'lodash';
import { Col, Row } from 'react-bootstrap';
import { branch } from 'baobab-react/higher-order';
import GenreSelect from './GenreSelect';
import ResultCard from './ResultCard';

const filterMovies = (movies, genreId) => {
	return filter(movies, movie => {
		if (movie.genre_ids) {
			return movie.genre_ids.includes(+genreId);
		}
		return false;
	})
}

const Results = ({ currentGenre, results }) => {

	let cards = <div className="no-results">Please Search for a movie</div>;

	if (results) {
		let movies = results;
		if (movies.length > 0) {
			if (currentGenre) {
				movies = filterMovies(movies, currentGenre);
			}
			const movieRows = chunk(movies, 4);
			cards = (
				<Row>
				{ 
					movieRows.map(row => (
						<Col sm={ 12 }>
							<Row>
							{
								row.map(movie => (
									<ResultCard key={ movie.id } movie={ movie } />
								))
							}
							</Row>
							<hr />
						</Col>
					)) 
				}
				</Row>
			);
		} else {
			cards = (
				<div className="no-results">No Results Found :(</div>
			);
		}
	}
	return (
		<div className="results">
			<Row>
				<Col sm={ 12 }>
					<h1>Results</h1>
					<GenreSelect currentGrenre={ currentGenre } hidden={ !results || results.length === 0 } />
				</Col>
			</Row>
			{ cards }
		</div>
	);
};

export default branch({
	currentGenre: ['currentGenre'],
	results: ['results']
}, Results);