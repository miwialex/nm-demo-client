import React from 'react';
import dateFormat from 'dateformat';
import { Button, Col, Thumbnail, Modal } from 'react-bootstrap';

class ResultCard extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			showModal: false
		}
	}

	close(event) {
		this.setState({ showModal: false });
	}

	open(event) {
		this.setState({ showModal: true });
	}

	render() {
		const { movie } = this.props;
		const image = movie.poster_path || 'http://placehold.it/185x278';
		let date = 'unknown';
		if (movie.release_date) {
			date = dateFormat(new Date(movie.release_date), 'mmmm dS, yyyy');
		}
		return (
			<Col xs={ 6 } sm={ 6 } md={ 3 } className="result-card">
				<Thumbnail src={ image } />
				<p><strong>{movie.title}</strong></p>
				<Button bsStyle="primary" bsSize="xsmall" onClick={ (e) => this.open(e) } block>Details</Button>
				<Modal show={this.state.showModal} onHide={ e => this.close(e) }>
				<Modal.Header closeButton>
					<Modal.Title>{ movie.title }</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<h5>Released: { date }</h5>
					<h5>Rating: { movie.vote_average }</h5>
					<hr />
					<p>{ movie.overview }</p>
				</Modal.Body>
				<Modal.Footer>
					<Button onClick={ e => this.close(e)}>Close</Button>
				</Modal.Footer>
			</Modal>
			</Col>
		);
	}
};

export default ResultCard;