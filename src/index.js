import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { root } from 'baobab-react/higher-order';
import tree from './state';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './site.css';

import searchService from './services/searchService';

searchService.getGenres().then(results => {
	tree.set('genres', results.genres);
});

const RootedApp = root(tree, App);

ReactDOM.render(
  <RootedApp />,
  document.getElementById('root')
);
