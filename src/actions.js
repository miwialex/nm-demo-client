import { find, matchesProperty, sortBy } from 'lodash';

import searchService from './services/searchService';

const buildGenreList = (tree, movies) => {
	const uniqueGenres = new Set();
	const allGenres = tree.get('genres');
	movies.forEach(movie => {
		movie.genre_ids.forEach(id => {
			const genre = find(allGenres, matchesProperty('id', id));
			if (genre) {
				uniqueGenres.add(genre);
			}
		});
	});
	tree.set('currentGenres', Array.from(uniqueGenres));

}

export function search(tree, search) {
	tree.set('currentGenre', '');
	searchService.search(search).then(results => {
		let movies = results.results;
		movies = sortBy(movies, ['title']);
		buildGenreList(tree, movies);
		tree.set('results', movies);
	}).catch(err => {
		alert('An error occured while searching');
	});
}

export function updateGenre(tree, genre) {
	tree.set('currentGenre', genre);
}