import request from 'superagent';

const searchService = {
	search(searchString) {
		return request
			.get('/api/v1/movie/search')
			.query({ query: searchString })
			.then(results => JSON.parse(results.text))
		;
	},

	getGenres() {
		return request
			.get('/api/v1/movie/genres')
			.then(results => JSON.parse(results.text))
		;
	}
};

export default searchService;