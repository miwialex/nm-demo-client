import React from 'react';
import { Grid, Navbar, NavbarBrand } from 'react-bootstrap';
import Header from './components/Header';
import MainContent from './components/MainContent';
import Footer from './components/Footer';

class App extends React.Component {
    render() {
        return (
            <div>
                <Navbar>
                    <NavbarBrand href="/">Movie Finder</NavbarBrand>
                </Navbar>
                <Grid>
                    <Header />
                    <MainContent />
                    <Footer />
                </Grid>
            </div>
        );
    }
}

export default App;
