# README #

### What is this repository for? ###

This is a demo React app to search Movies by title or keyword. In order to run this, you'll also need to install the demo API found [Here](https://bitbucket.org/miwialex/nm-demo-api)

### How do I get set up? ###

1. clone the API repo and run the API
1. clone the repo.
1. run `npm install` to install dependencies
1. run `npm run start` to start up the webapp
1. the app will be available at http://localhost:3000